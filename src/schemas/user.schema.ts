import { Prop, SchemaFactory, Schema } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { Role } from "src/user/roles/roles.enum";

export type UserDocument = HydratedDocument<User>

@Schema()
export class User {
    @Prop({ required: true })
    username: string

    @Prop({ required: true })
    password: string 

    @Prop({enum: Role, required: true })
    role: string
}

export const userSchema = SchemaFactory.createForClass(User)

