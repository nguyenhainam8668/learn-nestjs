import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cors from 'cors';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  /// Setting up Swagger
  const config = new DocumentBuilder()
    .setTitle('User example')
    .setDescription('The users API description')
    .setVersion('1.0')
    .addTag('user')
    .addTag("auth")
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  /// End setting up Swagger

  /// Set global prefix so that /api
  app.setGlobalPrefix('api');

  /// Apply cors
  app.use(cors());

  /// Listen to port
  await app.listen(8081);
}
bootstrap();
