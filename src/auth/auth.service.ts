import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from 'src/user/user.service';
import { JwtService } from '@nestjs/jwt';
import { SignInDto } from './dto/sign-in.dto';
import { SignUpDto } from './dto/sign-up.dto';
import * as bcrypt from 'bcrypt';

const saltOrRounds = 10;

@Injectable()
export class AuthService {
    constructor(
        private userService: UserService,
        private jwtService: JwtService
    ) {}

    signIn = async (signInDto: SignInDto) => {
        const { username, password } = signInDto
        if (!username || !password)
        {
            throw new Error("Missing fields")
        }

        const user = await this.userService.getByUsername(username)
        const passwordMatch = await bcrypt.compare(password, user.password)

        if (!user)
            throw new Error("User not found")

        else if (!passwordMatch)
            throw new UnauthorizedException()

        /// Sign In
        const payload = { sub: user._id, user}

        const accessToken = await this.jwtService.signAsync(payload, { secret: process.env.JWT_SECRET})

        return { accessToken }
    }

    signUp = async (signUpDto: SignUpDto) => {
        const { username, password, role } = signUpDto

        if (!username || !password || !role)
        {
            throw new BadRequestException("Missing fields")
        }

        else if (role != 'Admin' && role != 'User')
        {
            throw new BadRequestException("Role must be Admin or User")
        }

        const user = await this.userService.getByUsername(username)
        /// Existed username
        if (user)
        {
            throw new BadRequestException("Username existed.")
        }

        const hashPassword = await bcrypt.hash(password, saltOrRounds);

        const newUser = await this.userService.createNewUser({ username, password: hashPassword, role})
        if (!newUser)
        {
            throw new BadRequestException("Errors occur while signing up")
        }

        console.log(role)
        /// Sign In
        const payload = { sub: newUser._id, user: newUser}

        const accessToken = await this.jwtService.signAsync(payload)

        return { accessToken }    
    }
}
