import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsString } from "class-validator";
import { Role } from "src/user/roles/roles.enum";

export class SignUpDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    username: string;
    
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    password: string;

    @ApiProperty()
    @IsEnum(Role, { message: "Valid role required. User or Admin."})
    @IsNotEmpty()
    role: 'Admin' | 'User';   
} 