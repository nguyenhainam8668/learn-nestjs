import { BadRequestException, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from 'src/schemas/user.schema';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserService {
    constructor(@InjectModel(User.name) private userModel: Model<User>) {}

    

    async getAllUsers() {
        const users = await this.userModel.find()

        // console.log("All users", users)
        return users
    }

    async getById(id: string) {
        const user = await this.userModel.findById(id)

        // console.log(`User with id ${id} `, user)

        return user
    }

    async getByUsername(username: string) {
        const user = await this.userModel.findOne({ username })

        return user
    }

    async createNewUser(createUserDto: CreateUserDto) {
        const newUser = new this.userModel(createUserDto)
        await newUser.save()

        console.log("New user ", newUser)
        return newUser
    }

    async updateById(id: string, updateUserDto: UpdateUserDto) {
        const updatedUser = await this.userModel.findByIdAndUpdate(id, updateUserDto)

        console.log("Updated user", updatedUser)

        return updatedUser
    }

    async deleteById(id: string) {
        const deletedUser = await this.userModel.findByIdAndDelete(id)

        if (!deletedUser)
        {
            throw new BadRequestException("User not found")
        }

        console.log(`User ${deletedUser} deleted.`)

        return deletedUser  
    }
}
