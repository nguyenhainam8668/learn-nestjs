import { Injectable, ExecutionContext, CanActivate } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Observable } from "rxjs";

export class UserGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const { user: { sub, user }, params: { id }} = context.switchToHttp().getRequest()
        /// sub: userId of current user attached to request, id: user id in url (params)
        console.log("Current request user id and requested user id", sub, id)

        /// If admin, can get every user
        if (user.role === 'Admin')
            return true 
        /// User but get user not himself
        else if (sub !== id)
            return false

        return true
    }
}