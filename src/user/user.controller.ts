import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Role } from './roles/roles.enum';
import { Roles } from './roles/roles.decorator';
import { RolesGuard } from './roles/roles.guard';
import { AuthGuard } from 'src/auth/auth.guard';
import { UserGuard } from './user.guard';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('user')
@Controller('user')
export class UserController {
    constructor(private userService: UserService) {}
    /// GET /api/user
    @UseGuards(AuthGuard, RolesGuard)
    @Get()
    @Roles(Role.Admin)
    getAllUsers() {
        console.log("In /api/user...")
        return this.userService.getAllUsers()
    }

    /// GET /api/user/:id
    @UseGuards(AuthGuard, UserGuard)
    @Get(':id')
    getById(@Param('id') id: string) {
        return this.userService.getById(id)
    }

    /// POST /api/user
    @Post()
    createNewUser(@Body() createUserDto: CreateUserDto) {
        return this.userService.createNewUser(createUserDto)
    }

    /// PUT /api/user/:id
    @Put(':id')
    updateById(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto)
    {
        return this.userService.updateById(id, updateUserDto)
    }

    /// DELETE /api/user/:id
    @Delete(':id')
    deleteById(@Param('id') id: string) {
        return this.userService.deleteById(id)
    }

}
