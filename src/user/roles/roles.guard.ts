import { Injectable,CanActivate, ExecutionContext } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Observable } from "rxjs";
import { ROLES_KEY } from "./roles.decorator";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const requiredRoles = this.reflector.getAllAndOverride(ROLES_KEY, [
            context.getHandler(),
            context.getClass(),
        ])

        /// No require role to access
        if (!requiredRoles)
            return true 

        /// Require a role to access: e.g. Admin
        /// Get current user: req format: { sub: userId, user: User }
        const { user } = context.switchToHttp().getRequest().user

        console.log(user.role, requiredRoles)
        return requiredRoles.some((role: string) => role === user.role)
    }
}